## Overview

**futurae-web-python** - Provides the Futurae Web Python helpers to be integrated in your python web
application.

These helpers allow developers to integrate Futurae's Web authentication suite into their web apps,
without the need to implement the Futurae Auth API.

## Unit Tests

```bash
$ virtualenv ./venv
$ source ./venv/bin/activate
$ ./venv/bin/pip install -r requirements-dev.txt
$ nose2
```
## Documentation

Documentation on using the Futurae Web Widget can be found [here](https://futurae.com/docs/guide/futurae-web/).
