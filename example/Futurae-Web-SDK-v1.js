/**
 * Futurae Web SDK Version 1.0
 *
 * Copyright (C) 2018 Futurae Technologies AG - All rights reserved.
 * For any inquiry, contact: legal@futurae.com
 */

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define([], factory);

  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory();

  } else {
    var Futurae = factory();

    Futurae._onReady(Futurae.init);

    // root === window
    root.Futurae = Futurae;
  }
}(this, function() {
  var FUTURAE_MESSAGE_FORMAT = /^(?:AUTH|ENROLL)+\|[A-Za-z0-9\+\/=]+\|[A-Za-z0-9\+\/=]+$/;
  var FUTURAE_ERROR_FORMAT = /^ERR\|[\w\s\.\(\)]+$/;

  var iframeId = 'futurae_widget',
    postArgument = 'sig_response',
    postAction = '',
    lang = '',
    host,
    sigRequest,
    futuraeSig,
    appSig,
    iframe,
    submitCallback;

  function throwError(message) {
    throw new Error('Futurae Web SDK error: ' + message);
  }

  function hyphenize(str) {
    return str.replace(/([a-z])([A-Z])/, '$1-$2').toLowerCase();
  }

  // cross-browser data attributes
  function getDataAttribute(element, name) {
    if ('dataset' in element) {
      return element.dataset[name];
    }
    return element.getAttribute('data-' + hyphenize(name));
  }

  // cross-browser event binding
  function on(context, event, fallbackEvent, callback) {
    if ('addEventListener' in window) {
      context.addEventListener(event, callback, false);

    } else {
      context.attachEvent(fallbackEvent, callback);
    }
  }

  // cross-browser event unbinding
  function off(context, event, fallbackEvent, callback) {
    if ('removeEventListener' in window) {
      context.removeEventListener(event, callback, false);

    } else {
      context.detachEvent(fallbackEvent, callback);
    }
  }

  function onReady(callback) {
    on(document, 'DOMContentLoaded', 'onreadystatechange', callback);
  }

  function offReady(callback) {
    off(document, 'DOMContentLoaded', 'onreadystatechange', callback);
  }

  function onMessage(callback) {
    on(window, 'message', 'onmessage', callback);
  }

  function offMessage(callback) {
    off(window, 'message', 'onmessage', callback);
  }

  /**
   * Parse the sig_request parameter and throw the respective errors if the
   * signature contains a server error or if it is invalid.
   *
   * @param {String} sig Request signature
   */
  function parseSigRequest(sig) {
    if (!sig) {
      return;
    }

    if (sig.indexOf('ERR|') === 0) {
      throwError(sig.split('|')[1]);
    }

    if (sig.indexOf(':') === -1 || sig.split(':').length !== 2) {
      throwError(
        'Futurae was given an invalid signature. This indicates a configuration ' +
        'problem with one of Futurae\'s client libraries.'
      );
    }

    var sigParts = sig.split(':');

    sigRequest = sig;
    futuraeSig = sigParts[0];
    appSig = sigParts[1];

    return {
      sigRequest: sig,
      futuraeSig: sigParts[0],
      appSig: sigParts[1]
    };
  }

  /**
   * This function is setup to be executed when the DOM is ready and the iframe was
   * not available during `init`.
   */
  function onDOMReady() {
    iframe = document.getElementById(iframeId);

    if (!iframe) {
      throw new Error(
        'This page does not contain an iframe element that Futurae can use. ' +
        'Add an iframe, e.g. <iframe id="futurae_widget"></iframe> to this page. ' +
        'Check https://www.futurae.com/docs/ for more info.'
      );
    }

    ready();

    // cleanup
    offReady(onDOMReady);
  }

  /**
   * Validate that a MessageEvent originated from Futurae and it is has
   * a properly formatted payload.
   *
   * @param {MessageEvent} event Message received via postMessage
   */
  function isFuturaeMessage(event) {
    return Boolean(
      event.origin === ('https://' + host) &&
      typeof event.data === 'string' &&
      (
        event.data.match(FUTURAE_MESSAGE_FORMAT) ||
        event.data.match(FUTURAE_ERROR_FORMAT)
      )
    );
  }

  /**
   * Validate the request signature and setup the iframe.
   *
   * Every option defined below can be passed into an options hash argument to `Futurae.init`,
   * or specified on the iframe element using the `data-` attributes.
   *
   * Options specified using the options hash will take precedence over `data-` attributes.
   *
   * Example using options hash:
   * ```javascript
   * Futurae.init({
   *   iframe: "some_other_id",
   *   host: "api.futurae.com",
   *   sig_request: "<server_passed_signature",
   *   post_action: "/auth",
   *   post_argument: "resp",
   *   lang: "en"
   * });
   * ```
   *
   * Example using `data-` attributes:
   * ```
   * <iframe id="futurae_widget" data-host="api.futurae.com"
   *										         data-sig-request="..."
   *										         data-post-action="/auth"
   *										         data-post-argument="resp"
   *										         data-lang="en">
   * </iframe>
   * ```
   *
   * @param {Object} options
   * @param {String} options.host                           Futurae host to use
   * @param {String} options.sig_request                    Request signature
   * @param {String} options.iframe                         The iframe, or the id of an iframe element to setup
   * @param {String} [options.post_action='']               URL to POST back to after a successful authentication
   * @param {String} [options.post_argument='sig_response'] Parameter name to use for response signature
   * @param {String} [options.lang='']                      The language to be used throughout the widget, defaults to English
   * @param {Function} [options.submit_callback]            If provided, futurae will not submit the form, instead it will execute
   *                                                        the callback function. This can be used to prevent the reloading of the webpage.
   */
  function init(options) {
    if (options) {
      if (options.host) {
        host = options.host;
      }

      if (options.sig_request) {
        parseSigRequest(options.sig_request);
      }

      if (options.post_argument) {
        postArgument = options.post_argument;
      }

      if (options.post_action) {
        postAction = options.post_action;
      }

      if (options.lang) {
        lang = options.lang;
      }

      if (options.iframe) {
        if (options.iframe.tagName) {
          iframe = options.iframe;

        } else if (typeof options.iframe === 'string') {
          iframeId = options.iframe;
        }
      }

      if (typeof options.submit_callback === 'function') {
        submitCallback = options.submit_callback;
      }
    }

    if (iframe) {
      ready();

    } else {
      iframe = document.getElementById(iframeId);

      if (iframe) {
        ready();

      } else {
        onReady(onDOMReady);
      }
    }

    // cleanup
    offReady(init);
  }

  /**
   * This function is called when a message was received from another domain
   * using the `postMessage` API. Check first that the event came from Futurae,
   * and that the message has a properly formatted payload. Then post back to
   * the origin.
   *
   * @param event Event object (contains origin and data)
   */
  function onReceivedMessage(event) {
    if (isFuturaeMessage(event)) {
      doPostBack(event.data);

      // cleanup
      offMessage(onReceivedMessage);
    }
  }

  /**
   * Load the iframe from Futurae, then wait for it to postMessage back.
   */
  function ready() {
    if (!host) {
      host = getDataAttribute(iframe, 'host');

      if (!host) {
        throwError(
          'No API host is given. Be sure to pass a `host` parameter to Futurae.init, ' +
          'or through the `data-host` attribute on the iframe element.'
        );
      }
    }

    if (!futuraeSig || !appSig) {
      parseSigRequest(getDataAttribute(iframe, 'sigRequest'));

      if (!futuraeSig || !appSig) {
        throwError(
          'No valid signature request. Be sure to give the `sig_request` parameter ' +
          'to Futurae.init, or use the `data-sig-request` attribute on the iframe element.'
        );
      }
    }

    if (postArgument === 'sig_response') {
      postArgument = getDataAttribute(iframe, 'postArgument') || postArgument;
    }

    if (postAction === '') {
      postAction = getDataAttribute(iframe, 'postAction') || postAction;
    }

    if (lang === '') {
      lang = getDataAttribute(iframe, 'lang') || lang;
    }

    iframe.src = [
      'https://', host, '/frame/web/v1?sig=', futuraeSig,
      '&origin=', document.location.protocol, '//', encodeURIComponent(document.location.host),
      '&version=1.0',
      '&lang=', lang
    ].join('');

    onMessage(onReceivedMessage);
  }

  /**
   * POST back to the primary service with the response signature and any additional
   * user supplied parameters given in form#futurae_form.
   */
  function doPostBack(response) {
    var input = document.createElement('input');

    input.type = 'hidden';
    input.name = postArgument;
    input.value = response + ':' + appSig;

    // users can supply their own form with additional inputs
    var form = document.getElementById('futurae_form');

    if (!form) {
      form = document.createElement('form');

      // insert the form after the iframe element
      iframe.parentElement.insertBefore(form, iframe.nextSibling);
    }

    form.method = 'POST';
    form.action = postAction;

    form.appendChild(input);

    if (typeof submitCallback === "function") {
      submitCallback.call(null, form);

    } else {
      form.submit();
    }
  }

  return {
    init: init,
    _doPostBack: doPostBack,
    _isFuturaeMessage: isFuturaeMessage,
    _onReady: onReady,
    _parseSigRequest: parseSigRequest
  };
}));
