Simple example of a Python web server integrating Futurae Web authentication.

## Configuration

To set up, edit futurae.conf with the appropriate `wid`, `wkey`, `skey`, and `host` values.

## Installation

Set up a virtualenv and install the dependencies:

```
virtualenv .env
source .env/bin/activate
pip install -r requirements.txt
```

## Run the example

To run the server on port 8082, cd back to the root project directory and run:

```
python -m example.server
```

# Usage

Visit the root URL with a 'user' argument, e.g. 'http://localhost:8082/?username=myname'.
