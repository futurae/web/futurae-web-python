#
# futurae_web.py
#
# Copyright (C) 2018 Futurae Technologies AG - All rights reserved.
# For any inquiry, contact: legal@futurae.com
#

import base64
import hashlib
import hmac
import time

FUTURAE_EXPIRY = 600
APP_EXPIRY = 3600

WID_LEN = 36
WKEY_LEN = 40
SKEY_LEN = 40

REQUEST_PREFIX = 'REQ'
ENROLL_REQUEST_PREFIX = 'ENROLL_REQUEST'

AUTH_PREFIX = 'AUTH'
ENROLL_PREFIX = 'ENROLL'

APP_PREFIX = 'APP'

ERR_WID = 'ERR|The Futurae Web ID in the sign_request() call is invalid.'
ERR_WKEY = 'ERR|The Futurae Web key in the sign_request() call is invalid.'
ERR_SKEY = 'ERR|The secret key in the sign_request() call must be at least %s characters.' % SKEY_LEN
ERR_USERNAME = 'ERR|The username in the sign_request() call is invalid.'
ERR_USERNAME_ENCODING = 'ERR|The username in the sign_request() call could not be encoded. Pass a <unicode> username in Python 2 or <str> username in Python 3.'
ERR_UNKNOWN = 'ERR|An unknown error has occurred.'


def sign_request(wid, wkey, skey, username):
    """
    Generate a signed request for the Futurae authentication.
    The returned value should be passed to the Futurae.init() call
    in the rendered web page used for the Futurae authentication.
    Args:
    wid      - Futurae Web ID
    wkey     - Futurae Web key
    skey     - Secret key
    username - Authenticated username
    """
    return _sign_request(wid, wkey, skey, username, REQUEST_PREFIX)


def sign_enroll_request(wid, wkey, skey, username):
    """
    Generate a signed request for the Futurae enrollment.
    The returned value should be passed to the Futurae.init() call
    in the rendered web page used for the Futurae enrollment.
    Args:
    wid      - Futurae Web ID
    wkey     - Futurae Web key
    skey     - Secret key
    username - Authenticated username
    """
    return _sign_request(wid, wkey, skey, username, ENROLL_REQUEST_PREFIX)


def verify_response(wid, wkey, skey, sig_response):
    """
    Validate the signed response returned from Futurae.
    Returns the username of the authenticated user or None.
    Args:
    wid          - Futurae Web ID
    wkey         - Futurae Web key
    skey         - Secret key
    sig_response - The signed response POST'ed to the server
    """
    return _verify_response(wid, wkey, skey, AUTH_PREFIX, sig_response)


def verify_enroll_response(wid, wkey, skey, sig_response):
    """
    Validate the signed response returned from Futurae.
    Returns the username of the enrolled user or None.
    Args:
    wid          - Futurae Web ID
    wkey         - Futurae Web key
    skey         - Secret key
    sig_response - The signed response POST'ed to the server
    """
    return _verify_response(wid, wkey, skey, ENROLL_PREFIX, sig_response)


def _hmac_sha256(key, ptx):
    ctx = hmac.new(key, ptx, hashlib.sha256)
    return ctx.hexdigest()


def _sign_vals(key, vals, prefix, expiry):
    exp = str(int(time.time()) + expiry)

    ptx = '|'.join(vals + [exp])
    b64 = base64.b64encode(ptx.encode('utf-8')).decode('utf-8')
    cookie = '%s|%s' % (prefix, b64)

    sig = _hmac_sha256(key.encode('utf-8'), cookie.encode('utf-8'))
    return '%s|%s' % (cookie, sig)


def _parse_vals(key, val, prefix, wid):
    ts = int(time.time())

    u_prefix, u_b64, u_sig = val.split('|')
    if u_prefix != prefix:
        return None

    cookie = '%s|%s' % (u_prefix, u_b64)
    e_key = key.encode('utf-8')
    e_cookie = cookie.encode('utf-8')

    sig = _hmac_sha256(e_key, e_cookie)
    if _hmac_sha256(e_key, sig.encode('utf-8')) != _hmac_sha256(e_key, u_sig.encode('utf-8')):
        return None

    decoded = base64.b64decode(u_b64).decode('utf-8')
    u_wid, username, exp = decoded.split('|')

    if u_wid != wid:
        return None

    if ts >= int(exp):
        return None

    return username


def _sign_request(wid, wkey, skey, username, prefix):
    """
    Generate a signed request for Futurae authentication.
    The returned value should be passed into the Futurae.init() call
    in the rendered web page used for the Futurae authentication.
    Args:
    wid      - Futurae Web ID
    wkey     - Futurae Web key
    skey     - Secret key
    username - Authenticated username
    prefix   - REQUEST_PREFIX or ENROLL_REQUEST_PREFIX
    """
    try:
        username.encode('utf-8')
    except (UnicodeDecodeError, AttributeError):
        return ERR_USERNAME_ENCODING

    if not username or '|' in username:
        return ERR_USERNAME
    if not wid or len(wid) != WID_LEN:
        return ERR_WID
    if not wkey or len(wkey) < WKEY_LEN:
        return ERR_WKEY
    if not skey or len(skey) < SKEY_LEN:
        return ERR_SKEY

    vals = [wid, username]

    try:
        sig_futurae = _sign_vals(wkey, vals, prefix, FUTURAE_EXPIRY)
        sig_app = _sign_vals(skey, vals, APP_PREFIX, APP_EXPIRY)
    except Exception:
        return ERR_UNKNOWN

    return '%s:%s' % (sig_futurae, sig_app)


def _verify_response(wid, wkey, skey, prefix, sig_response):
    """
    Validate the signed response returned from Futurae.
    Returns the username of the authenticated user or None.
    Args:
    wid          - Futurae Web ID
    wkey         - Futurae Web key
    skey         - Secret key
    prefix       - AUTH_PREFIX or ENROLL_PREFIX that sig_response must match
    sig_response - The signed response POST'ed to the server
    """
    sig_futurae, sig_app = sig_response.split(':')

    try:
        user_futurae = _parse_vals(wkey, sig_futurae, prefix, wid)
        user_app = _parse_vals(skey, sig_app, APP_PREFIX, wid)
    except Exception:
        return None

    if user_futurae != user_app:
        return None

    return user_futurae
