#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''Tests for the Futurae Web Client'''

import sys
import unittest

import futurae_web
import responses

WID = 'FIDXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX'
WRONG_WID = 'FIDXXXXXXXXXXXXXXXXY'
WKEY = 'futuraegeneratedsharedwebapplicationkey.'
SKEY = 'useselfgeneratedhostapplicationsecretkey'
USERNAME = 'testusername'


class TestFuturaeWeb(unittest.TestCase):
    def setUp(self):
        sig_request = futurae_web.sign_request(WID, WKEY, SKEY, USERNAME)
        sig_futurae, self.sig_app_valid = sig_request.split(':')

        sig_request = futurae_web.sign_request(WID, WKEY, 'invalid' * 6, USERNAME)
        sig_futurae, self.sig_app_invalid = sig_request.split(':')

        sig_request = futurae_web.sign_enroll_request(WID, WKEY, 'invalid' * 6, USERNAME)
        sig_futurae, self.sig_enroll_invalid = sig_request.split(':')

        sig_request = futurae_web.sign_enroll_request(WID, WKEY, SKEY, USERNAME)
        sig_futurae, self.sig_enroll_valid = sig_request.split(':')

    def test_sign_request(self):
        sig_request = futurae_web.sign_request(WID, WKEY, SKEY, USERNAME)
        self.assertNotEqual(sig_request, None)

        sig_request = futurae_web.sign_request(WID, WKEY, SKEY, '')
        self.assertEqual(sig_request, futurae_web.ERR_USERNAME)

        sig_request = futurae_web.sign_request(WID, WKEY, SKEY, 'in|valid')
        self.assertEqual(sig_request, futurae_web.ERR_USERNAME)

        sig_request = futurae_web.sign_request('invalid', WKEY, SKEY, USERNAME)
        self.assertEqual(sig_request, futurae_web.ERR_WID)

        sig_request = futurae_web.sign_request(WID, 'invalid', SKEY, USERNAME)
        self.assertEqual(sig_request, futurae_web.ERR_WKEY)

        sig_request = futurae_web.sign_request(WID, WKEY, 'invalid', USERNAME)
        self.assertEqual(sig_request, futurae_web.ERR_SKEY)

    def test_sign_request_bytestring_username(self):
        """
        Tests that futurae_web will produce an encoding error for bytestring
        usernames in Python 3 (you cannot encode a bytes object in Python 3)
        and will reject bytestring usernames in PY2 if they contain non-ASCII
        characters.
        """
        if sys.version_info[0] == 2:
            username = bytes('testüsername')
        else:
            username = bytes('testüsername', 'utf-8')

        sig_request_bytestring = futurae_web.sign_request(WID, WKEY, WKEY, username)
        self.assertEqual(futurae_web.ERR_USERNAME_ENCODING, sig_request_bytestring)

    def test_verify_response_invalid(self):
        invalid_user = futurae_web.verify_response(
                WID, WKEY, SKEY, responses.invalid_response() + ':' + self.sig_app_valid)
        self.assertEqual(invalid_user, None)

        expired_response = responses.expired_response(WID, WKEY, USERNAME)
        expired_user = futurae_web.verify_response(
                WID, WKEY, SKEY, expired_response + ':' + self.sig_app_valid)
        self.assertEqual(expired_user, None)

        future_response = responses.future_response(WID, WKEY, USERNAME)
        future_user = futurae_web.verify_response(
                WID, WKEY, SKEY, future_response + ':' + self.sig_app_invalid)
        self.assertEqual(future_user, None)

        future_user = futurae_web.verify_response(
                WRONG_WID, WKEY, SKEY, future_response + ':' + self.sig_app_valid)
        self.assertEqual(future_user, None)

        bad_params_response = responses.bad_params_response(WID, WKEY, USERNAME)
        future_user = futurae_web.verify_response(
                WID, WKEY, SKEY, bad_params_response + ':' + self.sig_app_valid)
        self.assertEqual(future_user, None)

        bad_params_app = responses.bad_params_app(WID, SKEY, USERNAME)
        future_user = futurae_web.verify_response(
                WID, WKEY, SKEY, future_response + ':' + bad_params_app)
        self.assertEqual(future_user, None)

        future_enroll_response = responses.future_enroll_response(WID, WKEY, USERNAME)
        enroll_user = futurae_web.verify_enroll_response(
                WID, WKEY, SKEY, future_enroll_response + ':' + self.sig_enroll_invalid)
        self.assertEqual(enroll_user, None)

    def test_verify_response_valid(self):
        future_response = responses.future_response(WID, WKEY, USERNAME)
        future_user = futurae_web.verify_response(
                WID, WKEY, SKEY, future_response + ':' + self.sig_app_valid)
        self.assertEqual(future_user, USERNAME)

        future_enroll_response = responses.future_enroll_response(WID, WKEY, USERNAME)
        enroll_user = futurae_web.verify_enroll_response(
                WID, WKEY, SKEY, future_enroll_response + ':' + self.sig_enroll_valid)
        self.assertEqual(enroll_user, USERNAME)


if __name__ == '__main__':
    unittest.main()
