import base64
import hashlib
import hmac
import time


def bad_params_app(wid, skey, username):
    vals = [wid, username, _expired_expiry(), 'bad_param']
    cookie = _cookie('APP', vals)
    ctx_hex = hmac.new(skey, cookie, hashlib.sha256).hexdigest()
    return '%s|%s' % (cookie, ctx_hex)


def bad_params_response(wid, wkey, username):
    vals = [wid, username, _expired_expiry(), 'bad_param']
    cookie = _cookie('AUTH', vals)
    ctx_hex = hmac.new(wkey, cookie, hashlib.sha256).hexdigest()
    return '%s|%s' % (cookie, ctx_hex)


def expired_response(wid, wkey, username):
    vals = [wid, username, _expired_expiry()]
    cookie = _cookie('AUTH', vals)
    ctx_hex = hmac.new(wkey, cookie, hashlib.sha256).hexdigest()
    return '%s|%s' % (cookie, ctx_hex)


def future_response(wid, wkey, username):
    vals = [wid, username, _future_expiry()]
    cookie = _cookie('AUTH', vals)
    ctx_hex = hmac.new(wkey, cookie, hashlib.sha256).hexdigest()
    return '%s|%s' % (cookie, ctx_hex)


def future_enroll_response(wid, wkey, username):
    vals = [wid, username, _future_expiry()]
    cookie = _cookie('ENROLL', vals)
    ctx_hex = hmac.new(wkey, cookie, hashlib.sha256).hexdigest()
    return '%s|%s' % (cookie, ctx_hex)


def invalid_response():
    return 'AUTH|INVALID|SIG'


def _cookie(prefix, vals):
    return '%s|%s' % (prefix, base64.b64encode('|'.join(vals)))


def _future_expiry():
    return str(int(time.time()) + 600)


def _expired_expiry():
    return str(int(time.time()) - 600)
